### Mybatis逆向工程

#### 功能

- 可一次生成多张表
- model与example分别为两个包

#### 使用说明

> 使用前请修改`generator.properties`中的配置信息

```properties
#驱动jar地址
drive_path=C:\\Users\\chenaijun\\.m2\\repository\\mysql\\mysql-connector-java\\5.1.25\\mysql-connector-java-5.1.25.jar
#驱动名称
driver=com.mysql.cj.jdbc.Driver
#数据库连接地址
url=jdbc:mysql://localhost:3306/member?useSSL=false&useUnicode=true&characterEncoding=utf-8&serverTimezone=GMT%2B8
#数据库账号
username=root
#数据库密码
password=root
#数据库名
schema=member
#表名（多个表用逗号隔开）
table_name=dim_achievements,goods,historical_payment,member,menu,menu_tmp,order,order_report_form,user
#实体名（多个实体用逗号隔开）
entity_name=DimAchievements,Goods,HistoricalPayment,Member,Menu,MenuTmp,Order,OrderReportForm,User
#实体类生成包名
model_package=com.generator.template.model
#mapper.xml生成文件夹
mapperXml_location=E:\\IdeaPro\\workspace\\template\\src\\main\\resources\\
#mapper接口生成包名
dao_package=com.generator.template.mapper
#指定需要生成的项目路径（需提前创建一个空的项目）
java_location=E:\\IdeaPro\\workspace\\template\\src\\main\\java\\
```

- 驱动地址为本地
- 表名可一次创建多个，之间用逗号隔开
- 实体名如上
- Example包名为：实体类包名中的model替换为example
- 指定需要生成的项目路径，可以使用模板：https://gitlab.com/pursuej/template.git