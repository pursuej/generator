package com.generator;

import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.internal.DefaultShellCallback;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class GeneratorApplication {

    public static void main(String[] args) throws FileNotFoundException {
        List<String> war=new ArrayList<>();
        boolean ovr=true;
        File file= ResourceUtils.getFile("classpath:generator.xml");
        ConfigurationParser cp=new ConfigurationParser(war);
        try {
            Configuration config=cp.parseConfiguration(file);
            DefaultShellCallback back=new DefaultShellCallback(ovr);
            MyBatisGenerator my=new MyBatisGenerator(config, back, war);
            my.generate(null);
            for(String s:war) {
                System.out.println(s);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
