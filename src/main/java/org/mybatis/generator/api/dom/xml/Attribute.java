//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package org.mybatis.generator.api.dom.xml;

public class Attribute implements Comparable<Attribute> {
    private String name;
    private String value;

    public Attribute(String name, String value) {
        if (name.contains("parameterType") && value.contains("Example")) {
            value = value.replace("model", "example");
        }
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return this.name;
    }

    public String getValue() {
        return this.value;
    }

    public String getFormattedContent() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.name);
        sb.append("=\"");
        sb.append(this.value);
        sb.append('"');
        return sb.toString();
    }

    public int compareTo(Attribute o) {
        if (this.name == null) {
            return o.name == null ? 0 : -1;
        } else {
            return o.name == null ? 0 : this.name.compareTo(o.name);
        }
    }
}
